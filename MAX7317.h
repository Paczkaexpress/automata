#include <stdint.h>
#include "lib/automata_spi.h"

#ifndef MAX7317_H_
#define MAX7317_H_

//#define DEBUG //logging data for debugging purposes

#define MAX7317_MESSAGE_LENGHT 16
#define MAX7317_BITRATE 1000000 
#define READ_FLAG 1
#define WRITE_FLAG 0

#define SET_PORT 1
#define RESET_PORT 0

#define SET_AS_OUTPUT 0
#define SET_AS_INPUT 1

SPI_RETURN_T SPI_HardwareInit();
void MAX7317_SetPort(uint8_t, uint8_t);
int8_t MAX7317_ReadPort(uint8_t);
SPI_RETURN_T MAX7317_SendMessage(uint8_t, uint8_t, uint8_t);
SPI_RETURN_T MAX7317_GetMessage(uint8_t, uint8_t, uint8_t);
void MAX7317_logFile(SPI_RETURN_T);
#endif
