#include "MAX7317.h"
#include <stdint.h>
#include "stdio.h"

enum MAX7317_AddressMap
{
	portP0OutputLevel = 0x00,
	portP1OutputLevel = 0x01,
	portP2OutputLevel = 0x02,
	portP3OutputLevel = 0x03,
	portP4OutputLevel = 0x04,
	portP5OutputLevel = 0x05,
	portP6OutputLevel = 0x06,
	portP7OutputLevel = 0x07,
	portP8OutputLevel = 0x08,
	portP9OutputLevel = 0x09,
	writeAllPorts 	  = 0x0A,
	readLSBPorts	  = 0x0E,
	readMSBPorts      = 0x0F
	
};

SPI_RETURN_T SPI_HardwareInit()
{
	SPI_CONFIG_T SPI_cfg;
	SPI_cfg.cs_pol = SPI_ACTIVE_LOW; //low state during transmission
	SPI_cfg.cpol = SPI_IDLE_LOW; //low clock state when idle
	SPI_cfg.cpha = SPI_EDGE_RISING; // data latch on rising edge
	SPI_cfg.bits_per_frame = MAX7317_MESSAGE_LENGHT;
	SPI_cfg.baudrate = MAX7317_BITRATE;
	SPI_cfg.cs_to_sck = 10; //according to datasheet 9.5, however int thus bigger
	SPI_cfg.sck_to_cs = 3; //according to datasheet 2.5, however int thus bigger

	return SPI_configureTransfer(&SPI_cfg);
}

int8_t MAX7317_ReadPort(uint8_t portNumber)
{
	int8_t portValue = 0;

	if(portNumber <= 7)
	{
		portValue = (MAX7317_GetMessage(READ_FLAG,readLSBPorts,0) & (1<<portNumber))>>portNumber;
	}
	else if(portNumber <= 9)
	{
		portValue = MAX7317_GetMessage(READ_FLAG,readLSBPorts,0) & (1<<(portNumber-7))>>portNumber; 
	}
	else
	{
#ifdef DEBUG
		printf("Port index out of range");
#endif
		portValue = -1;
	}
	
	return portValue;
}

void MAX7317_SetPort(uint8_t portNumber, uint8_t state)
{
	if(portNumber <= 9)
	{
		MAX7317_SendMessage(WRITE_FLAG,portNumber,state);
	}
	else
	{
#ifdef DEBUG
		printf("Port index out of range. Not possible to set up a port");
#endif
	}
}


SPI_RETURN_T MAX7317_SendMessage(uint8_t readFlag, uint8_t address, uint8_t data)
{
	SPI_TRANSFER_T SPI_tx;
	SPI_RETURN_T SPI_return;

	uint8_t txBuff[2];
	uint8_t rxBuff[2];

	txBuff[0] = readFlag<<7 | address;
	txBuff[1] = data;

	SPI_tx.tx_data = *(&txBuff);
	SPI_tx.rx_data = *(&rxBuff);
	SPI_tx.number_of_bytes = 2;

	SPI_return = SPI_transferBlocking(&SPI_tx);
	MAX7317_logFile(SPI_return);
	return SPI_return;
}

SPI_RETURN_T MAX7317_GetMessage(uint8_t readFlag, uint8_t address, uint8_t data)
{
	SPI_TRANSFER_T SPI_tx;
	SPI_RETURN_T SPI_return;

	uint8_t txBuff[2];
	uint8_t rxBuff[2];

	txBuff[0] = (readFlag<<7) | address;
	txBuff[1] = data;

	SPI_tx.tx_data = *(&txBuff);
	SPI_tx.rx_data = *(&rxBuff);
	SPI_tx.number_of_bytes = 2;
	
	SPI_return = SPI_transferBlocking(&SPI_tx);
	MAX7317_logFile(SPI_return);
#ifdef DEBUG
	printf("%d %d", SPI_tx.rx_data[0],SPI_tx.rx_data[1]);
#endif
	return SPI_return;
}

void MAX7317_logFile(SPI_RETURN_T state) // simple error logger 
{
#ifdef DEBUG
	if(state == SPI_RETURN_ok)
	{
		printf("OK/success \n-");
	}
	else if(state == SPI_RETURN_nullptr)
	{
		printf("Argument contains a null pointer \n");
	}
	else if(state == SPI_RETURN_invalid_arg)
	{
		printf("Argument out of range/invalid \n");
	}
	else
	{
		printf("Undefined Error \n");
	}
#endif
}

